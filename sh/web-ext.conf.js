module.exports = {
    // Global options:
    verbose: true,
    ignoreFiles: [
        'py/**',
        'sh/**',
        'bin/**'
    ],
    // Command options:
    build: {
        overwriteDest: true,
    }
};