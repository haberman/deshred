import json
import sys
import datetime

from progress.bar import IncrementalBar
from math import ceil


def time_cover(input_list, key_compare):
    lower = float('Inf')
    upper = -float('Inf')

    for item in input_list:
        ts = round(float(item[key_compare]) * 1e-3)
        lower = min(lower, ts)
        upper = max(upper, ts)

    return {'min': lower, 'max': upper}


def time_closest(input_list, value, key_compare):
    return min(input_list, key=lambda x: abs(round(float(x[key_compare]) * 1e-3)-value))


def degrade_jump(locations, jumps):
    ''' Reduces input list by jumping over a given amount of locations. '''

    i = 0
    out = []
    while i < len(locations):
        out.append(locations[i])
        i = i + jumps

    return out


def degrade_clamp(locations, amount, on_ts=False):
    print('clamp, ts:{}'.format(on_ts))
    ''' Clamp input list to keep a maximum `amount` of locations. If `on_ts` is `True`, compression is made by a linear time progression. '''

    if on_ts is False:
        return degrade_jump(locations, ceil(len(locations)/amount))
    else:
        print('TODO')
        return None


def degrade_frequency(locations, freq):
    ''' Reduces input list by picking locations at a regular frequency. '''

    cover = time_cover(locations, 'timestampMs')
    seconds = cover['max'] - cover['min']
    steps = round(seconds / freq)

    bar = IncrementalBar('Processing', max=steps)
    i = cover['min']
    out = []

    while i < cover['max']:
        closest = time_closest(locations, i, 'timestampMs')
        out.append(closest)
        i = i + freq
        bar.next()

    return out


def degrade_vaa(locations, vaa_file):
    ''' Filter srource input by crossing target on a given vaa's list. '''

    vaas = json.load(open(vaa_file))
    bar = IncrementalBar('Processing', max=len(vaas))
    out = []

    i = 0
    for vaa in vaas:
        new_vaa = vaa
        ts = round(float(vaa['id']) * 1e-6)
        closest = time_closest(locations, ts, 'timestampMs')
        new_vaa['location'] = pack_location(closest)

        out.append(new_vaa)
        bar.next()

        # if i >= 100:
        #     break

        i = i + 1

    return out


def pack_location(location, minify=True):
    new_location = {'ts': round(float(location['timestampMs']) * 1e-3),
                    'lat': float(location['latitudeE7']) * 1e-7,
                    'lon': float(location['longitudeE7']) * 1e-7,
                    'heading': location.get('heading', 'n/a'),
                    'altitude': location.get('altitude', 'n/a')}

    activities = location.get('activity', 'n/a')

    if activities is not 'n/a':
        activities = activities[0]['activity']

        if minify:
            sort = sorted(activities, key=lambda a: a['confidence'])
            new_location['activity'] = sort[0]['type']
        else:
            new_location['activity'] = activities

    return new_location


def pack_export(locations, minify=True):
    ''' Projects `IE7` locations to lat/lng pair. If `minify` is `True`, replace acticity array by the most relevant one. '''

    out = []
    for location in locations:
        out.append(pack_location(location, minify))

    return out


LOCATIONS = json.load(open('locations_raw.json'))['locations']
JUMPS = 12
CLAMP = 5000
EXTRACT = {'start': 0, 'stop': ceil(len(LOCATIONS) * .01)}
FREQ = 3600 * 6  # 4 locations a day

# filename = 'altered/jump_{}.json'.format(JUMPS)
# filename = 'altered/clamp_{}.json'.format(CLAMP)
# filename = 'altered/extract_{}:{}.json'.format(EXTRACT['start'],
#                                                EXTRACT['stop'])
# filename = 'altered/freq_{}.json'.format(FREQ)
filename = 'altered/cross_vaa.json'

with open(filename, 'w+') as outfile:
    # locations = degrade_jump(LOCATIONS, JUMPS)
    # locations = degrade_clamp(LOCATIONS, CLAMP)
    # locations = LOCATIONS[EXTRACT['start']:EXTRACT['stop']]
    # locations = degrade_frequency(LOCATIONS, FREQ)

    locations = degrade_vaa(LOCATIONS, 'vaa_source.json')
    json.dump(locations, outfile, indent=2)

    ratio = len(locations) / len(LOCATIONS)
    print('{} locations ({:.0%} of input) have been written to {} '.format(len(locations),
                                                                           ratio, filename))
