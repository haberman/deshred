/**
 * Responds to network requests (`main_frame` & `xmlhttprequest`).
 * This is where data deshreding takes place.
 * 
 * @param {browser.webRequest} browser_request A `webRequest` instance
 * @param {array}              domains An array of domains to reduce network listening range and temper CPU [defaults: `<all_urls>`]
 * @param {function}           data_callback A function to be called when we have parsed valid data
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest
 */
const network =
    (browser_request, domains, data_callback) => {
        if (data_callback && typeof data_callback == 'function') {
            data_callback_ = data_callback
        }

        const urls = domains == null ? ['<all_urls>'] : domains.map(item => `https://*${item}/*`)

        return {
            listen: value => network_listen_(browser_request, value, urls),
            listening: () => listening_,
            has_listener: () => network_has_listener_(browser_request)
        }
    }


const REGEX_VAA_HOOK = /(HISTORY_response=\')(.*)(\';window.)/
const REGEX_UNHEX = /\\x([0-9a-f]{2})/g

/** A list of sub-states to indicate what type of URLs is watched. */
const LISTENING_IDLE = 0
const LISTENING_VAA = 1
const LISTENING_LOCATION = 2

/** Holds one of the above `int` constant for the actual listening state. */
let listening_ = LISTENING_IDLE

/** Data exchanges come in shreds, this holds value until it becomes `JSON` parseable. */
let data_stream_ = ''

/** Method to be called whenever we parse valid data. */
let data_callback_ = null

/**
 * `onBeforeRequest` callback
 * -> pipes relevant requests into bytes' streams into parsers...
 * 
 * @param {object} details Details about the request
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest/onBeforeRequest#details
 */
const network_listener_ =
    details => {
        data_stream_ = ''
        const filter = browser.webRequest.filterResponseData(details.requestId)

        filter.onstop = event => {
            data_stream_ = ''
            filter.disconnect()
        }

        filter.ondata =
            async event => {
                const decoder = new TextDecoder('utf-8')
                let plain_shred = decoder.decode(event.data)

                switch (listening_) {
                    case LISTENING_VAA:
                        const items = merge_vaa_(plain_shred, details.type)
                        if (items && data_callback_) { data_callback_({ edge: 'vaa', data: items }) }
                        break
                    case LISTENING_LOCATION:
                        // TODO -> merge_location_(plain_shred)
                        console.log(details)
                        console.log(plain_shred)
                        break
                }

                filter.write(event.data)
            }

        filter.onerror = event => { console.warn(`filter error`) }

        return {}
    }
/**
 * Adds or removes webRequest listeners.
 * 
 * @param {boolean} value Whether to listen to the network or not
 * @param {array}   urls Array urls to filter requests listening
 */
const network_listen_ =
    (request, value, urls) => {
        listening_ = value
        const has_listener = network_has_listener_(request)

        if (listening_ != LISTENING_IDLE && !has_listener) {
            const filter = { urls: urls, types: ['xmlhttprequest', 'main_frame'] }
            request.onBeforeRequest.addListener(network_listener_, filter, ["blocking"])
        } else if (has_listener) {
            request.onBeforeRequest.removeListener(network_listener_)
        }
    }

/** Shortcuts to know of network is busy / listening. */
const network_has_listener_ = request => request.onBeforeRequest.hasListener(network_listener_)

const merge_vaa_ =
    (incoming, type) => {
        let items = null

        if (type == 'main_frame') { items = parse_html_vaa_(incoming) }
        else if (type == 'xmlhttprequest') { items = parse_xhr_vaa_(data_stream_ + incoming) }

        // parsing has failed by maybe because of an incomplete array string, so append current plain to partial shred
        if (items == null) {
            if (data_stream_ == '') { data_stream_ = incoming.substring(5) }
            else { data_stream_ += incoming }
        }

        return items

    }
/**
 * HTML parser.
 * 
 * @param {string} plain_shred Plain html chunk
 * @returns {array} An array of formatted vaa or null in case of failure.
 */
const parse_html_vaa_ =
    plain_shred => {
        const matches = plain_shred.match(REGEX_VAA_HOOK)

        // immediate rejection is there's no or invalid matches
        if (!matches) { return null }

        // unhex array string
        const unhex_shred = matches[2].replace(REGEX_UNHEX, (match, contents, offset, input_string) => {
            const sub_hex = input_string.substring(offset, offset + contents.length + 2)
            return decode_hexdec_(sub_hex)
        })

        return parse_xhr_vaa_(unhex_shred)
    }

/**
 * Takes a plain string object and formats an array of voice & audio activity items with downloadable links.
 *
 * @param {string} string_array A plain piece of text that's supposed to represent an array
 */
const parse_xhr_vaa_ =
    string_array => {
        let json_array = null
        let deshreds = null

        // internal iterator that consumes Google's fat array
        const traverse = (obj) => {
            if (typeof obj == 'object') {
                Object.entries(obj).forEach(([key, value]) => {
                    if (value && typeof value == 'object') {

                        // turns out vaa items are encoded in array of relatively constant size
                        if (value.length == 25 || value.length == 26) {
                            if (deshreds == null) { deshreds = [] }

                            // map value indices to keys
                            const deshred = {
                                id: value[4],
                                date: new Date(value[4].substring(0, value[4].length - 6) * 1000),
                                context: value[7][0],
                                action: value[9][2],
                                transcript: value[9][0],
                                response: value[9][3],
                                audio: value[24][0]
                            }

                            if (value.length == 26) { deshred.trigger = value[25][0][0] }

                            deshreds.push(deshred)
                        }

                        traverse(value)
                    }
                })
            }
        }

        try { json_array = JSON.parse(string_array) }
        catch (e) { return null }

        traverse(json_array)
        return deshreds
    }

/** Returns the string representation of an hexadecimal (\x) symbol. */
const decode_hexdec_ =
    hex_string => {
        let str = ''
        for (var i = 0; i < hex_string.length; i += 2) {
            const v = parseInt(hex_string.substr(i, 2), 16)
            if (v) { str += String.fromCharCode(v) }
        }
        return str
    }
