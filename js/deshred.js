const URL_VAA = 'https://myactivity.google.com/myactivity?restrict=vaa'
const URL_LOC = 'https://www.google.com/maps/timeline'

/** Holds a port which enables communications with the popup. */
let port_ = null

/** Shortcut to get the active tab. */
const ux_active_tab =
    async () => {
        const active_tab = await browser.tabs.query({ active: true, currentWindow: true })
        return active_tab[0]
    }

/**
* Update the plugin interface, meaning: icon state, badge count and popup DOM (in case we have an open port on it).
*
* @param {object} settings Stored settings
* @param {array} vaa Stored voice & audio activites
*/
const ux_update =
    (settings, vaa) => {
        if (vaa) {
            browser.browserAction.setBadgeBackgroundColor({ color: 'rgb(75,75,75)' })
            browser.browserAction.setBadgeText({ text: vaa.length.toString() })
        }

        if (port_ && !browser.runtime.lastError) {
            let download_content = browser.i18n.getMessage('ui_noitem')

            if (vaa) { download_content = browser.i18n.getMessage('ui_items', [vaa.length.toString()]) }
            else { /* TODO */ }

            const reply = {
                settings: settings,
                count: vaa ? vaa.length : 0,
                download_content: download_content,
                reload_vaa_title: browser.i18n.getMessage('ui_reload_vaa'),
                reload_loc_title: browser.i18n.getMessage('ui_reload_loc'),
                download_title: browser.i18n.getMessage('ui_download_archive')
            }

            port_.postMessage({ from: 'background', action: 'update', reply: reply })
            return true
        }

        return false
    }

/**
 * Callback to navigation change.
 * -> starts / stops network sniffing depending on the URL.
 */
const ux_navigation =
    async listening => {
        const active_tab = await ux_active_tab()
        const stored_settings = await storage_.settings()

        let network_listening = network_.listening()

        if (listening == -1) {
            const vaa_url = active_tab.url.indexOf(URL_VAA)
            const loc_url = active_tab.url.indexOf(URL_LOC)

            if (vaa_url != -1) { network_listening = LISTENING_VAA }
            else if (loc_url != -1) { network_listening = LISTENING_LOC }
            else { network_listening = LISTENING_IDLE }
        }

        if (network_listening != LISTENING_IDLE || listening != -1) {
            await action_.state(STATE_LISTEN)
            await storage_.state(STATE_LISTEN)

            if (!network_.has_listener()) { network_.listen(listening == -1 ? network_listening : listening) }
            if (listening == -1) { await browser.tabs.reload(active_tab.id) }
            else {
                console.log('YOP')
                const url = listening == LISTENING_VAA ? URL_VAA : URL_LOC
                await browser.tabs.update(active_tab.id, { url: url })
            }
        } else {
            await action_.state(STATE_DEFAULT)
            await storage_.state(STATE_DEFAULT)

            network_.listen(LISTENING_IDLE)
        }

        return active_tab.id
    }

/** 
 * Packs what's available into a zip that's immediately asked for download.
 * Also posts messages about the audio files' download progress.
 * 
 * @param {object} traits An array of traits 
 * @param {object} settings The current stored settings
 * @returns {boolean} This will return `false` is case of download failure or data unavailability.
*/
const ux_download =
    async vaa => {
        const folder = `deshred_${Date.now()}`
        const zip = new JSZip()
        zip.folder(folder)

        if (vaa) {
            zip.file(`${folder}/vaa.json`, data_to_json(vaa))
            zip.file(`${folder}/vaa.csv`, data_to_csv(vaa))

            // we're gonna download the vaa files so stop listening to google while we use its network (TODO [@polish] this is nice, but useless since we're not listening to media type)
            let restore_listening = network_.listening()
            if (restore_listening != LISTENING_IDLE) { network_.listen(LISTENING_IDLE) }

            let i = 0
            for (let item of vaa) {
                let audio_fetch = null

                try { audio_fetch = await fetch(item.audio, { credentials: 'include' }) } // cookie session credentials needs to be forwarded
                catch (e) { continue }

                const audio_blob = await audio_fetch.blob()
                zip.file(`${folder}/vaa_${item.id}.mp3`, audio_blob)

                ++i
                if (port_ && !browser.runtime.lastError) {
                    const reply = {
                        type: 'progress',
                        msg: browser.i18n.getMessage('info_downloading', [i, vaa.length]),
                        exit: i == vaa.length ? -1 : 1
                    }

                    port_.postMessage({ from: 'background', action: 'log', reply: reply })
                }
            }

            // restore listening
            if (restore_listening != LISTENING_IDLE) { network_.listen(restore_listening) }
        }

        // TODO -> location, search

        if (vaa) {
            const blob = await zip.generateAsync({ type: 'blob' })
            const url = URL.createObjectURL(blob)

            try { await browser.downloads.download({ url: url, filename: `${folder}.zip` }) }
            catch (e) { return false }
        } else {
            return false
        }

        return true
    }

/**
 * Method passed to the network as a callback to valid data reception.
 * 
 * @param {object} info An object holding an `edge` key telling us what to do with `data`
 */
const data_callback =
    async info => {
        if (!info.hasOwnProperty('edge')) { return false }

        const stored_settings = await storage_.settings()
        switch (info.edge) {
            case 'vaa':
                const stored_vaa = await storage_.sync_vaa(info.data)
                const handle = await ux_update(stored_settings, stored_vaa)
                return handle
                break

            // TODO -> location
        }

        return true
    }

/**
* Encodes an array of objects into a blob of its corresponding csv representation.
* For every objects, the biggest one will be used as a reference to inject an header at the beginning of the blob.
* 
* @param {array} items The array to encode
* @returns {string} A csv encoded string
*/
const data_to_csv =
    items => {
        // 1st pass -> gets biggest header
        let header = null
        items.map(
            item => {
                const keys = Object.keys(item)
                if (header == null || header.length < keys.length) { header = keys }
            })

        // 2nd pass -> writes data
        let csv = ''
        items.map(
            item => {
                csv += Object.keys(item).reduce((previous, key) => previous + item[key] + ',', '') + '\r\n'
            })
        return `${header.join(',')}\r\n${csv}`
    }

/** Little JSON beautifier.  */
const data_to_json = items => JSON.stringify(items, null, 2)

const storage_ = storage(browser.storage, browser.i18n.getUILanguage())
const action_ = action(browser.browserAction)
const network_ = network(browser.webRequest, null, data_callback)

/**
 * When plugin gets installed
 * -> inits storage and restores ui.
 */
browser.runtime.onInstalled.addListener(
    async e => {
        await storage_.init()
        await action_.init()
        return true
    })

/**
 * When user changes active tab
 * -> grabs a new URL to attach / detach network's listener if it matches REGEX_MYACTIVITY.
 */
browser.tabs.onActivated.addListener(
    async e => {
        const handle = await ux_navigation(-1)
        return handle
    }
)

/**
 * When plugin action button gets clicked
 * -> stores new state and refresh ui.
 */
browser.browserAction.onClicked.addListener(
    async e => {
        const state = await action_.click()
        await storage_.state(state)
        return true
    })

/**
 * When document is loaded, we're missing the first items because data in injected within an html script of the final page (aka `main_frame`)
 * -> informs network so it will start parsing xmlhttprequest over html content once DOM content is loaded.
 */
// browser.webNavigation.onCompleted.addListener(
//     async e => {
//         network_.prefer_frame(false)
//         const handle = await ux_navigation()
//         return handle
//     })

/**
 * Listens to messages posted on background port and triggers actions upon'em:
 * -> when issued from popup: returns stats to refresh its DOM or pack storage as a file to be downloaded.
 */
browser.runtime.onConnect.addListener(
    port => {
        port_ = port

        port.onMessage.addListener(
            async request => {
                if (request.target == 'popup') {
                    const stored_settings = await storage_.settings()
                    const stored_vaa = await storage_.vaa()

                    switch (request.action) {
                        case 'open':
                            return ux_update(stored_settings, stored_vaa)
                            break
                        case 'reload-vaa':
                            await ux_navigation(LISTENING_VAA)
                            break
                        case 'reload-loc':
                            await ux_navigation(LISTENING_LOCATION)
                            break
                        case 'download':
                            const download = await ux_download(stored_vaa, stored_settings)
                            if (!download) {
                                const reply = {
                                    type: 'warn',
                                    msg: browser.i18n.getMessage('info_nodata')
                                }

                                port_.postMessage({ from: 'background', action: 'log', reply: reply })
                            }

                            break

                    }

                    return true
                }

                return false
            })


        port.onDisconnect.addListener(
            port => {
                port_ = null;
                if (port.error) { console.warn(`disconnected because of error: ${port.error}`) }
            })
    })
