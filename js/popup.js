/** Connects to background and posts an open status update. */
const port = browser.runtime.connect(browser.runtime.getManifest().applications.gecko.id)
port.postMessage({ target: 'popup', action: 'open' })

const INFO_VISIBILITY_TIMEOUT = 5000 // 2 lines of info

/** Popup logging should not be dismissed after being diplayed. */
const LOG_EXIT_NONE = 1

/** Popup should be forced to hide. */
const LOG_EXIT_ON = -1

/** Popup will be hidde after 5s. */
const LOG_EXIT_DELAY = 0

/**
 * Shows popup log div with a new message.
 * 
 * @param {string} msg The text to display
 * @param {string} type The type of logging (will be used to add css classes)
 * @param {int} exit An exit code to indicate whether and when the popup should be dismissed
 */
const log =
    (msg, type, exit) => {
        const popup_log = document.querySelector('div.popup-log')

        if (exit == LOG_EXIT_ON) {
            popup_log.classList.add('hidden')
            return false
        }

        popup_log.classList.add(`log-${type}`)
        popup_log.classList.remove(`hidden`)
        popup_log.textContent = msg

        if (exit == LOG_EXIT_DELAY) { window.setTimeout(() => popup_log.classList.add('hidden'), INFO_VISIBILITY_TIMEOUT) }
    }

/** Updates stats with new info from background script. */
const update =
    info => {
        const settings = info.reply.settings

        let icon = '../assets/'
        if (settings.state) {
            icon += 'deshred_listen.svg'
        } else {
            icon += 'deshred.svg'
        }

        document.querySelector('html').setAttribute('lang', settings.locale.substring(0, 2))
        document.querySelector('a.button-microphone').setAttribute('title', info.reply.reload_vaa_title)
        document.querySelector('a.button-placeholder').setAttribute('title', info.reply.reload_loc_title)
        document.querySelector('#state_icon').setAttribute('data', icon)

        const bt_download = document.querySelector('a.button-download')
        bt_download.textContent = info.reply.download_content
        bt_download.setAttribute('data-count', info.reply.count)
        bt_download.setAttribute('title', info.reply.download_title)
    }

/** 
 * Answers to update requests.
 * -> updates DOM
 */
port.onMessage.addListener(
    info => {
        if (info.action == 'update') { update(info) }
        else if (info.action == 'log') { log(info.reply.msg, info.reply.type, info.reply.exit || LOG_EXIT_DELAY) }
    })

/** When DOM is loaded, starts listening to relevant events. */
window.addEventListener('DOMContentLoaded',
    () => {

        document.querySelector('a.button-download')
            .addEventListener('click', e => port.postMessage({ target: 'popup', action: 'download', format: 'zip' }))

        document.querySelector('a.button-microphone')
            .addEventListener('click', e => port.postMessage({ target: 'popup', action: 'reload-vaa' }))

        document.querySelector('a.button-placeholder')
            .addEventListener('click', e => port.postMessage({ target: 'popup', action: 'reload-loc' }))
    })
