/**
 * Handles interactions with the persistent storage API so we can
 * keep a track of user settings and resources.
 * 
 * @param {browser.storage} object For now storage use the `local` flavor of browser storage
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage 
 */
const storage = (browser_storage, locale) => {
    return {
        init: () => storage_init_(browser_storage, locale),
        settings: () => storage_settings_(browser_storage),
        state: value => storage_state_(browser_storage, value),
        vaa: () => storage_vaa_(browser_storage),
        sync_vaa: (value) => storage_sync_vaa_(browser_storage, value)
    }
}

const KEY_SETTINGS = 'deshred_settings'
const KEY_VAA = 'deshred_vaa'

/** Inits by storing defaults settings. */
const storage_init_ =
    async (storage, locale) => {
        const default_settings = { state: STATE_DEFAULT, locale: locale }

        await storage_clear_(storage)
        await storage.local.set({ deshred_settings: default_settings })

        const stored_settings = await storage_settings_(storage)
        return stored_settings
    }

/**
 * Stores a new state.
 * 
 * @param {int} value The new state as an integer
 */
const storage_state_ =
    async (storage, value) => {
        let stored_settings = await storage_settings_(storage)
        stored_settings.state = value
        await storage.local.set({ deshred_settings: { state: value, locale: stored_settings.locale } })
        return stored_settings
    }

/** Returns the deshred stored settings. */
const storage_settings_ =
    async storage => {
        const stored_settings = await storage.local.get(KEY_SETTINGS)

        if (stored_settings.hasOwnProperty(KEY_SETTINGS)) { return stored_settings[KEY_SETTINGS] }
        else { return null }
    }

const storage_vaa_ =
    async storage => {
        const stored_vaa = await storage.local.get(KEY_VAA)

        if (stored_vaa.hasOwnProperty(KEY_VAA)) { return stored_vaa[KEY_VAA] }
        else { return null }
    }

/** Clears both settings and vaa entire storage. */
const storage_clear_ =
    async storage => {
        await storage.local.remove(KEY_SETTINGS)
        await storage.local.remove(KEY_VAA)

        return true
    }

/**
 * Syncs an incoming array of fresh vaa items and stores them if not already present.
 * 
 * @param {array} value The actual content of stored vaa items
 */
const storage_sync_vaa_ =
    async (storage, value) => {
        let stored_vaa = await storage_vaa_(storage)

        // immediately stores the first time we hit storage
        if (!stored_vaa) {
            await storage.local.set({ deshred_vaa: value })
            stored_vaa = await storage_vaa_(storage)
        }

        if (stored_vaa == null) { return null }

        // filters incoming items to keep uniques only
        const incoming_vaa = []
        for (let vaa of value) {
            if (!stored_vaa.find(el => el.id == vaa.id)) { incoming_vaa.push(vaa) }
        }

        // concats old with new and stores
        await storage.local.set({ deshred_vaa: stored_vaa.concat(incoming_vaa) })

        stored_vaa = await storage_vaa_(storage)
        return stored_vaa
    }
