
# Deshred 0.7.4
## A Firefox plugin that reconstructs a private database of diverse Google activities.
For now, we're only reconstructing Google voice & audio activity by listening network when navigating [there](https://myactivity.google.com/myactivity?restrict=vaa).
The goal is to further extend the plugin to location and search history.

### Using
- **dev**: clone repo or download archive, type exactly `about:debugging` in Firefox and locate `manifest.json` by clicking on `Load Temporary Add-on`. Enable add-on debugging and click `Debug` to see logs;
- **install**: the plugin is now publically listed on [mozilla](https://addons.mozilla.org/en-US/firefox/addon/deshred/) and every versions are kept as signed `xpi` under the [bin](https://framagit.org/haberman/deshred/tree/master/bin) directory.

### Todo's
1. [x] make it actually work (we're only getting network traffic headers for now, not content)
        [oh shit](https://www.moesif.com/blog/technical/apirequest/How-We-Captured-AJAX-Requests-with-a-Chrome-Extension/) -> try [this](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest/StreamFilter).
2. [x] implement a shred parser with data coming from `main_frame`.
3. [x] implement a shred parser with data coming from `xmlhttprequest` when scrolling page.
4. [x] unclutter raw data into meaningful key/value object.
5. [x] code API to sync valid data with storage.
6. [x] fixe ajax parser with data coming after user clicks for details (where there's other vaa to be found).
7. [x] #ui add a badge with the current amount of stored vaa items.
8. [x] #ui make a browser action popup so we could get info & download file represention of stored content.
9. [x] #ui make a link to the [vaa](https://myactivity.google.com/myactivity?restrict=vaa) inside popup.
10. [x] implement data format (`json` / `csv`) export choice.
11. [x] add timestamp to generated file name.
12. [x] add a `zip` export option to make an archive of text data.
13. [x] #ui implement i18n support.
14. [x] #v1 dowloads audio and pack them with the archive. [13/04] -> fuck! we need credentials for that so be the man in the middle and steal session?! naaah... we're goog! Just had to forward credentials using `{ credentials: 'include' }` when fetching source.
- [ ] when browsing vaa, only `xmlhttprequest` return data; so investigate to make the plugin able to grab new items when scrolling down;
- [ ] #v1 cross-reference the unshreded voice & audio activity data with location's history;
- [ ] #v++ cross-reference with every Google activities.
- [ ] #v++ inject some CSS DOM modification when landed on the Google's vaa page to visually flag items for which we have a stored clone;
- [ ] #v++ since we're using the `download` API, check if it's possible to sync new data with an history;
- [ ] #v++ adds a feature to export an offline web page with some data visualization.

### History
- [29/03/2018] -> solved `1` by using [webRequest.filterResponseData](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest/filterResponseData), which makes the plugin only compatible with Firefox >= 57.
- [30/03/2018] -> checked `2`, `3`, `4`, `5`, `6`.
- [31/03/2018] -> checked `7`, `8`, `9`, `10`, `11`.
- [12/04/2018] -> checked `12` & `13` by aligning some code to the sibling [adverser](https://framagit.org/haberman/adverser/) plugin.
- [13/04/2018] -> checked `14`.